
options(scipen = 999999999999, OutDec = ",")

#Caso não tenha rodado, rodar
#source("R/nota_tec_1_mensais.R")

#funções úteis
pegadez <- function(tabela){
  tabela%>%dplyr::filter(substr(AAMM,5,6) == 12)
}

mm <- function(elem,t = 12) {
  round(frollmean(elem,t , align = "right"),0)
}


saitab <- function(tan) {
  a <- paste0(tan,"_a")
  t <- pegadez(get(a))
  
  t %<>% mutate(AAMM = substr(AAMM,1,4)) %>%rename(Ano = AAMM)
#  writexl::write_xlsx(t,paste0("resultados/nt_01/",tan,"_anual.xlsx"))
  t
}


#Situação ocupacional DF 1992-2021
r_s_a  <- ped9221 %>% 
  dplyr::filter(idade >= 14) %>%
  mutate(sit2 = case_when(sit == 0 ~ "Indefinido",
                          sit == 1 ~ "Desemprego oculto precário",
                          sit == 2 ~ "Desemprego aberto",
                          sit == 3 ~ "Desemprego oculto desalento",
                          sit == 4 ~ "Ocupado",
                          sit == 5 ~ "Inativo com trabalho excepcional",
                          sit == 6 ~ "Inativo puro",
                          sit == 7 | sit == 10 ~ "Não se aplica"
  )
  ) %>%
  group_by(AAMM, sexo,sit2) %>%
  summarize(total = sum(peso)) 

r_s_a <- r_s_a %>%
  group_by(sexo,sit2) %>%
  summarize(AAMM,total = round(frollmean(total, 12, align = "right"), 0)) %>%
dplyr::filter(!is.na(total)) %>%
   pivot_wider(names_from = c(sit2,sexo), values_from = total)

r_s_anual <- saitab("r_s")

r_s_anual %>% kbl(format.args = list(big.mark = ".", decimal.mark =","))  %>%
  kable_styling( font_size = 11,)

#PIA DF 1992-2021
pia_s_a  <- ped9221 %>% 
  dplyr::filter(idade >= 14) %>%
  group_by(AAMM,sexo) %>%
  summarize(total = sum(peso)) %>%
  pivot_wider(names_from = sexo,values_from = total)%>%ungroup()%>%
  mutate(across(-AAMM,mm))%>%
  rename(Homens_pia = Homens, Mulheres_pia = Mulheres)

pia_s_anual <- saitab("pia_s")

# pia_anual %>% kbl(format.args = list(big.mark = ".", decimal.mark =","))  %>%
#   kable_styling( font_size = 11,)

#População Ocupada DF 1992-2021
po_s_a  <- ped9221 %>% 
  dplyr::filter(idade >= "14") %>%
  dplyr::filter(sit == "4") %>%
  group_by(sexo,AAMM) %>%
  summarize(total = sum(peso)) %>%
  summarize(AAMM,sexo, MM_po = round(frollmean(total, 12, align = "right"), 0)) 

po_s_anual <- saitab("po_s")


#População Desocupada DF 1992-2021
pd_s_a  <- ped9221 %>% 
  dplyr::filter(idade >= "14") %>%
  dplyr::filter(sit == "1" | sit == "2" | sit == "3") %>%
  mutate(sit2 = case_when(sit == 1 ~ "Desemprego oculto precário",
                          sit == 2 ~ "Desemprego aberto",
                          sit == 3 ~ "Desemprego oculto desalento"
  )
  ) %>%
  group_by(sexo,AAMM, sit2) %>%
  summarize(total = sum(peso)) %>%
  group_by(sit2) %>%
  summarize(AAMM, sexo,MM_total = round(frollmean(total, 12, align = "right"), 0)) %>%
  dplyr::filter(!is.na(MM_total)) %>%
  pivot_wider(names_from = c(sexo,sit2), values_from = MM_total)

pd_s_a$`População Desocupada`  <- rowSums(pd_s_a%>%select(-1), na.rm = T)

pd_s_anual <- saitab("pd_s")


#Força de trabalho ou População Economicamente Ativa DF 1992-2021

pea_s_a  <- ped9221 %>% 
  dplyr::filter(idade >= 14) %>%
  dplyr::filter(sit == 1 | sit == 2 | sit == 3 | sit == 4) %>%
  mutate(sit2 = case_when(sit == 1 ~ "Desemprego oculto precário",
                          sit == 2 ~ "Desemprego aberto",
                          sit == 3 ~ "Desemprego oculto desalento",
                          sit == 4 ~ "Ocupado")
  ) %>%
  group_by(AAMM, sit2,sexo) %>%
  summarize(total = sum(peso)) %>%
  pivot_wider(names_from = c(sexo,sit2), values_from = total)%>%ungroup()%>%
  mutate(across(-AAMM, mm) )


pea_s_a$`Força de Trabalho`  <- rowSums(pea_s_a%>%select(-1), na.rm = T)


pea_s_anual <- saitab("pea_s")


#População Inativa DF 1992-2021
pinatv_s_a  <- ped9221 %>% 
  dplyr::filter(idade >= "14") %>%
  dplyr::filter(sit == "5" | sit == "6") %>%
  mutate(sit2 = case_when(sit == 5 ~ "Inatividade pura",
                          sit == 6 ~ "Inativ. trabalho ocasional"
  )
  ) %>%
  group_by(sexo,AAMM, sit2) %>%
  summarize(total = sum(peso)) %>%
  pivot_wider(names_from = c(sexo,sit2), values_from = total)%>%ungroup()%>%
  mutate(across(-AAMM, mm))

pinatv_s_a$`População inativa total`  <- rowSums(pinatv_s_a%>%select(-1), na.rm = T)

pinatv_s_anual <- saitab("pinatv_s")



#Taxa de atividade (PIA / população) 1992-2021
tativ_s_a  <- ped9221 %>%
  group_by(sexo,AAMM) %>%
  mutate(ppia = ifelse(idade<14,0,1))%>%
  summarize(pop_total = sum(peso), pia = sum(peso*ppia))%>%
  pivot_longer(-(1:2),names_to = "indicador", values_to = "valor") %>%
  pivot_wider(names_from = c(sexo,indicador), values_from = valor)%>%
  mutate(across(-1,mm)) %>%dplyr::filter(!is.na(Mulheres_pia))


tativ_s_anual <- saitab("tativ_s")

#Taxa de participação (PEA/PIA) 1992-2021
tpart_s_a  <- ped9221 %>%
  group_by(AAMM,sexo) %>%
  mutate(ppia = ifelse(idade<14,0,1))%>%
  mutate(ppea = ifelse(sit == 0 | sit > 4,0,1))%>%
  summarize(pea = sum(peso*ppea), pia = sum(peso*ppia))%>%
pivot_longer(-(1:2),names_to = "indicador", values_to = "valor") %>%
  pivot_wider(names_from = c(sexo,indicador), values_from = valor) %>%
ungroup()%>%  mutate(across(-1,mm))%>%
  dplyr::filter(!is.na(Homens_pia))%>%
  mutate(
         taxa_part_h = round(100*Homens_pea/Homens_pia,2),
         taxa_part_f = round(100*Mulheres_pea/Mulheres_pia,2))



tpart_s_anual <- saitab("tpart_s")

#Taxa de Inatividade (pop. inativa/PIA) 1992-2021
tinativ_s_a  <- ped9221 %>%
  dplyr::filter(idade >= 14)%>%
  group_by(AAMM,sexo) %>%
  mutate(pinativ = ifelse(sit ==  5 | sit == 6,1,0))%>%
  summarize(inativ = sum(peso*pinativ), pia = sum(peso)) %>%
  pivot_longer(-(1:2),names_to = "indicador", values_to = "valor") %>%
  pivot_wider(names_from = c(sexo,indicador), values_from = valor) %>%
ungroup()%>%  mutate(across(-1,mm))%>%
  dplyr::filter(!is.na(Homens_pia))%>%
  mutate(
    taxa_part_h = round(100*Homens_inativ/Homens_pia,2),
    taxa_part_f = round(100*Mulheres_inativ/Mulheres_pia,2))

tinativ_s_anual <- saitab("tinativ_s")


#Nível de Ocupação (PO/PIA)  1992-2021
nocup_s_a  <- ped9221 %>%
  group_by(AAMM,sexo) %>%
  mutate(ppia = ifelse(idade<14,0,1))%>%
  mutate(ppo = ifelse(sit == 4,1,0))%>%
  summarize(po = sum(peso*ppo), pia = sum(peso*ppia)) %>%
  pivot_longer(-(1:2),names_to = "indicador", values_to = "valor") %>%
  pivot_wider(names_from = c(sexo,indicador), values_from = valor) %>%
  ungroup()%>%  mutate(across(-1,mm))%>%
  dplyr::filter(!is.na(Homens_pia))%>%
  mutate(
    nivel_ocupacao_h = round(100*Homens_po/Homens_pia,2),
    nivel_ocupacao_f = round(100*Mulheres_po/Mulheres_pia,2))


nocup_s_anual <- saitab("nocup_s")


#Taxa de Ocupação (PO/PEA)  1992-2021
tocup_s_a  <- ped9221 %>%
  group_by(AAMM,sexo) %>%
  mutate(ppea = ifelse(sit == 0 | sit > 4,0,1))%>%
  mutate(ppo = ifelse(sit == 4,1,0))%>%
  summarize(po = sum(peso*ppo), pea = sum(peso*ppea)) %>%
  pivot_longer(-(1:2),names_to = "indicador", values_to = "valor") %>%
  pivot_wider(names_from = c(sexo,indicador), values_from = valor) %>%
  ungroup()%>%  mutate(across(-1,mm))%>%
  dplyr::filter(!is.na(Homens_pea))%>%
  mutate(
    tx_ocupacao_h = round(100*Homens_po/Homens_pea,2),
    tx_ocupacao_f = round(100*Mulheres_po/Mulheres_pea,2))


tocup_s_anual <- saitab("tocup_s")

#Taxa de Desocupação (PD aberta/PEA) e (PD total/PEA) 1992-2021
tdocup_s_a  <- ped9221 %>%
  group_by(AAMM,sexo) %>%
  mutate(ppea = ifelse(sit == 0 | sit > 4,0,1))%>%
  mutate(ppd1 = ifelse(sit == 2,1,0))%>%
  mutate(ppd2 = ifelse(sit > 0 & sit < 4,1,0))%>%
  summarize(pd_aberto = sum(peso*ppd1), 
            pd_total = sum(peso*ppd2), 
            pea = sum(peso*ppea)) %>%
  pivot_longer(-(1:2),names_to = "indicador", values_to = "valor") %>%
  pivot_wider(names_from = c(sexo,indicador), values_from = valor) %>%
  ungroup()%>%  mutate(across(-1,mm))%>%
  dplyr::filter(!is.na(Homens_pea))%>%
  mutate(
    tx_desemprego_total_h = round(100*Homens_pd_total/Homens_pea,2),
    tx_desemprego_aberto_h = round(100*Homens_pd_aberto/Homens_pea,2),
    tx_desemprego_total_f = round(100*Mulheres_pd_total/Mulheres_pea,2),
    tx_desemprego_aberto_f = round(100*Mulheres_pd_aberto/Mulheres_pea,2))

tdocup_s_anual <- saitab("tdocup_s")

#Trabalhadores por posição na ocupação principal - agregado parecido a Miragaya

#1992 a 2011
# 3, 4, 6  - Assalariado/direto p/ Setor Público (estat e não estat e autônomo p. Setor Público)
# 1, 2, 5 - Assalariado privado
#  8, 11 - Empresário (Empregador, Dono)
# 7, 9, 10, 12 - Autônomo para empresa, Doméstico, Familiar,  e outros

#2012 a 2021
# 3, 5  - Assalariado/direto p/ Setor Público (estat e não estat e autônomo)
# 1, 2, 4 - Assalariado privado
# 7, 11 - Empresário
# 8:10 , 12 - Autônomo para empresa, Doméstico, Familiar,  e outros


trab_ggrupos_s_a <- ped9221 %>% select(AAMM, sexo,pos_ocup, peso)

trab_ggrupos_s_a%<>%
  group_by(AAMM,sexo,pos_ocup)%>% summarize(na_posicao = sum(peso,na.rm = T))%>%
  pivot_wider(names_from = c(sexo,pos_ocup), values_from = na_posicao) %>%
  mutate(across(-1,mm))

trab_ggrupos_s_anual <- saitab("trab_ggrupos_s")


#população total
pop_total_s_a <- ped9221%>%group_by(AAMM,sexo)%>%summarize("População Total" = sum(peso,na.rm = T))%>%
  pivot_longer(-1:-2, names_to ="indicador", values_to = "valor")%>%
  pivot_wider(names_from = c(sexo,indicador), values_from = valor)

pop_total_s_a %<>% ungroup()%>%mutate(across(-1,mm))%>%dplyr::filter(!(is.na(`Homens_População Total`)))


pop_total_s_anual <- saitab("pop_total_s")

#Tabela 1 - Alguns indicadores de atividade 

tab1s <- pop_total_s_anual%>%left_join(pia_s_anual)%>%left_join(pea_s_anual)%>%left_join(tdocup_s_anual)%>%
  left_join(tativ_s_anual)%>%left_join(tinativ_s_anual)

tab1s <- tab1s%>%select(-`Força de Trabalho`,-contains("pd_aberto"),-contains("pop_total"))
#-contains("pia")

#rever
# names(tab1s)[c(3,8:14)] <- c("População em idade ativa\n(PIA)","Total de desempregados", "População economicamente ativa\n(PEA)",
#                             "Taxa de desemprego aberto","Taxa de desemprego total","Taxa de atividade","Inativos", "Taxa de inatividade" )


#tab1$dt <- as.Date(paste0(tab1$Ano,1231),"%Y%m%d")

#tab1 <-tab1%>% select(ncol(tab1),1:(ncol(tab1)-1))

#tab1 <- xts::xts(tab1,tab1$dt)

tab1s %<>%mutate(`cresc.(%) Pop. Total Masculina` = round(100*(`Homens_População Total` -dplyr::lag(`Homens_População Total`))/`Homens_População Total`,1),
                `Taxa de cresc(%) PEA Masc` = round(100*(`Homens_pea` -dplyr::lag(Homens_pea))/Homens_pea,1),
                `Taxa de cresc(%) PIA Masc` = round(100*(Homens_pia -dplyr::lag(Homens_pia))/Homens_pia,1),
`cresc.(%) Pop. Total Feminina` = round(100*(`Mulheres_População Total` -dplyr::lag(`Mulheres_População Total`))/`Mulheres_População Total`,1),
`Taxa de cresc(%) PEA Feminina` = round(100*(`Mulheres_pea` -dplyr::lag(Mulheres_pea))/Mulheres_pea,1),
`Taxa de cresc(%) PIA Feminina` = round(100*(Mulheres_pia -dplyr::lag(Mulheres_pia))/Mulheres_pia,1))

#write_xlsx(tab1s,"resultados/nt_01/demografia_e_mercado_de_trabalho_DF_sexo_bruto.xlsx")
#Miragaya 




#Tabela 5 - Distrito Federal - Evolução do mercado de trabalho

tab5_s <- pea_s_anual%>%select(Ano,contains('Ocupado'),contains('Desempreg'),`Força de Trabalho`)

#tab5_s$Desempregados <- rowSums(tab5_s[3:5],na.rm = T)

tab5_s <- tab5_s%>%left_join(trab_ggrupos_anual%>%select(-6))

tab5_s %<>% transmute(Ano,`Pessoal ocupado total` = Ocupado,
                    `Assal./Aut. setor público`,
                    `Assal. privado`,
                    `Empresário`,
                    `Domésticas, demais autônomos e outros`,
                    Desempregados, 
                    `Força de Trabalho`,
                    `Taxa de Desemprego` = paste0(round(100*Desempregados/`Força de Trabalho`,1),"%"))

#write_xlsx(tab5_s,"resultados/nt_01/evolucao_do_mercado_de_trabalho_s.xlsx")


#evolução força de trabalho sem grande grupo
#1992 a 2011
# 3, 4, 6  - Assalariado/direto p/ Setor Público (estat e não estat e autônomo p. Setor Público)
# 1, 2, 5 - Assalariado privado
#  8, 11 - Empresário (Empregador, Dono)
# 7, 9, 10, 12 - Autônomo para empresa, Doméstico, Familiar,  e outros

#2012 a 2021
# 3, 5  - Assalariado/direto p/ Setor Público (estat e não estat e autônomo)
# 1, 2, 4 - Assalariado privado
# 7, 11 - Empresário
# 8:10 , 12 - Autônomo para empresa, Doméstico, Familiar,  e outros

trab_s_grupos_a <- ped9221 %>% select(AAMM, POS, peso)%>%
  mutate(posicao = case_when(
    
    AAMM<201600 & POS == 1 ~ "Assal. privado com carteira", 
    AAMM<201600 & POS == 2 ~ "Assal. privado sem carteira", 
    AAMM<201600 & POS == 3 ~ "Assal. setor público Estatutário",
    AAMM<201600 & POS == 4 ~ "Assal. setor público não Estatutário",
    AAMM<201600 & POS == 5 ~ "Assal. prov sem carteira",
    AAMM<201600 & POS == 6 ~ "Autônomo p/ setor público",
    AAMM<201600 & POS == 7  ~ "Autônomo p/ empresa",
    AAMM<201600 & POS == 8  ~ "Empresário",
    AAMM<201600 & POS == 9  ~ "Doméstica/o",
    AAMM<201600 & POS == 10 ~ "Trabalhador Familiar",
    AAMM<201600 & POS == 11  ~ "Empresário",
    AAMM<201600 & POS == 12 ~ "Outras posições",
    
    AAMM>201200 & POS == 1  ~ "Assal. privado com carteira",
    AAMM>201200 & POS == 2 ~ "Assal. privado sem carteira",
    AAMM>201200 & POS == 3  ~ "Assal. setor público",
    AAMM>201200 & POS == 4 ~ "Assal. privado prov. sem carteira",
    AAMM>201200 & POS == 5 ~ "Autônomo para o Público",
    AAMM>201200 & POS == 6 ~ "Autônomo para Empresas",
    AAMM>201200 & POS == 7 ~ "Empresário",
    AAMM>201200 & POS == 8 ~ "Domésticas/os mensalistas",
    AAMM>201200 & POS == 9 ~ "Domésticas/os diaristas",
    AAMM>201200 & POS == 10 ~ "Trabalhador Familiar",
    AAMM>201200 & POS == 11 ~ "Empresário",
    AAMM>201200 & POS == 12 ~ "Outras posições",
    T ~ "não se aplica"))
trab_grupos_s_a%<>%
  group_by(AAMM,posicao)%>% summarize(na_posicao = sum(peso,na.rm = T))%>%
  pivot_wider(names_from = posicao, values_from = na_posicao)

trab_grupos_s_anual <- saitab("trab_grupos_s")


#write_xlsx(trab_s_grupos_anual,"resultados/nt_01/evolucao_do_mercado_de_trabalho_grupos_det_sexo.xlsx")
