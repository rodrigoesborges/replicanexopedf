
##carrega funções úteis
source("R/utils.R")

#pega colunas correspondentes a variaveis adicionais de renda
source("R/dic_var_adic_renda.R")
# 1. Conexão

# conexão
db <- DBI::dbConnect(odbc(),
                     "DB_CODEPLAN",
                     uid="codeplan",
                     pwd="codeplan")


pedspmb <- paste0("ped.NovaPEDPMB",2020:2022)
npmbtab <- str_to_lower(gsub("ped.Nova","",pedspmb))

lapply(1:length(pedspmb),function(x) {assign(npmbtab[x],dbGetQuery(db,paste("SELECT * FROM",pedspmb)[x]), envir = .GlobalEnv)})

colspedpmb <- c("CONGLOM","C030", "C050", "FATOR", "AAMM", 
                     "POS", "SIT","F300","C040","CNAE","SETOR_CNAE","F470","F481","F220","F210",colsr_2018,"INFLATORDF","INST")

clspmb <- list("pmb2020" = str_to_lower(colspedpmb),"pmb2021" = str_to_lower(colspedpmb), "pmb2021" = colspedpmb)

lapply(1:length(pedspmb),function(x) {assign(paste0(npmbtab[x],"a"),get(npmbtab[x])[unlist(clspmb[x])], envir = .GlobalEnv)})

#renomeia em utils.R adaptada
ren_pmb <- function(nome){
  a <- get(nome)
  names(a) <- names(pedpmb2022a)
  assign(nome,a, envir = .GlobalEnv)
}
lapply(paste0(npmbtab,"a"),ren_pmb)

pmb2022 <- rbindlist(lapply(ls(pattern="pedpmb\\d{4}a"),get))

rm(list = ls(pattern="pedpmb\\d{4}v*a"))

rm(list = ls(pattern="pedpmb2\\d{3}v*"))

saveRDS(pmb2022,"dados/ped_pmb_todos_varsbd.rds")

#pmb2022 <- read_rds("dados/ped_todos_varsbd.rds")
## 2.1 Mutate criando novas colunas, comuns a todas as PEDs selecionadas


pmb2022 <- pmb2022 %>% 
  rename(idade = C050, sexo = C030, peso = FATOR, etnia = C040, pos_ocup = POS,sit = SIT,
         ramo = CNAE, setor = SETOR_CNAE)%>%
  mutate(sexo=case_when(sexo == 1 ~ "Homens",
                        sexo == 2 ~ "Mulheres",
                        T ~ 'outros/indef/n.resp'))  

#### Faixas etárias
nfxet <- c("até 15 anos", "15 a 24 anos","25 a 29 anos","30 a 39 anos", "40 a 49 anos","50 a 59 anos","60 anos ou mais" )
pmb2022$faixa_et <- cut(pmb2022$idade,c(0,15,25,30,40,50,60,130),labels = nfxet,right = F)
#### Faixas etárias


#### Etnias
pmb2022 <- pmb2022%>%mutate(etnia = as_factor(case_when(etnia == 1 ~ "Branca",
                                                        etnia == 2 ~ "Preta",
                                                        etnia == 3 ~ "Parda",
                                                        etnia == 4 ~ "Amarela",
                                                        etnia == 5 & AAMM > 201500 ~ "Indígena",
                                                        etnia == T ~ "Não sabe/Sem declaração")))
#### Etnias


#### Situação de atividade
pmb2022%<>%mutate(sit2 = case_when(sit == 0 ~ "Indefinido",
                                   sit == 1 ~ "Desemprego oculto precário",
                                   sit == 2 ~ "Desemprego aberto",
                                   sit == 3 ~ "Desemprego oculto desalento",
                                   sit == 4 ~ "Ocupado",
                                   sit == 5 ~ "Inativo com trabalho excepcional",
                                   sit == 6 ~ "Inativo puro",
                                   sit == 7 | sit == 10 ~ "Não se aplica"
))

#### Situação de atividade




#### Posição na ocupação - agregada


pmb2022%<>%mutate(posicao = case_when(
  pos_ocup == 1  ~ "Assal. privado",
  pos_ocup == 2 ~ "Assal. privado",
  pos_ocup == 3  & F300 == 2 & AAMM>201606 ~ "Assal. setor público",#"Assal. setor público",
  pos_ocup == 3 & F300 == 1 & AAMM<201607 ~ "Assal. setor público",#"Assal. setor público",
  pos_ocup == 3  & F300 != 2 & AAMM>201606 ~ "Assal. setor público",#"Assal. setor público", pergunta estatutario até 2017 F260 renomeada
  pos_ocup == 3  & F300 != 1 & AAMM<201607 ~ "Assal. setor público",#"Assal. setor público", pergunta estatutario até 2017 F260 renomeada
  pos_ocup == 4 ~ "Assal. n.i.",
  pos_ocup == 5 ~ "Autônomo",
  pos_ocup == 6 ~ "Autônomo",
  pos_ocup == 7 ~ "Empresário",
  pos_ocup == 8 ~ "Doméstica/os e outros",#"Domésticas/os mensalistas",
  pos_ocup == 9 ~ "Doméstica/os e outros",#"Domésticas/os diaristas",
  pos_ocup == 10 ~ "Trabalhador Familiar e outras posições",
  pos_ocup == 11 ~ "Empresário",
  pos_ocup == 12 ~ "Trabalhador Familiar e outras posições",
  T ~ "não se aplica"))

#### Posição na ocupação - agregada



#### Posição na ocupação - detalhada

pmb2022 %<>%mutate(posicao_det = case_when(
  pos_ocup == 1  ~ "Assal. privado com carteira",
  pos_ocup == 2 ~ "Assal. privado sem carteira",
  pos_ocup == 3  & F300 == 2 & AAMM>201606 ~ "Assal. setor público Estatutário",#"Assal. setor público",
  pos_ocup == 3 & F300 == 1 & AAMM<201607 ~ "Assal. setor público Estatutário",#"Assal. setor público",
  pos_ocup == 3  & F300 != 2 & AAMM>201606 ~ "Assal. setor público não Estatutário",#"Assal. setor público", pergunta estatutario até 2017 F260 renomeada
  pos_ocup == 3  & F300 != 1 & AAMM<201607 ~ "Assal. setor público não Estatutário",#"Assal. setor público", pergunta estatutario até 2017 F260 renomeada
  pos_ocup == 4 ~ "Assal. n.i.",
  pos_ocup == 5 ~ "Autônomo p/ pf",
  pos_ocup == 6 ~ "Autônomo p/ empresa",
  pos_ocup == 7 ~ "Empresário",
  pos_ocup == 8 ~ "Doméstica/o mensalista",#"Domésticas/os mensalistas",
  pos_ocup == 9 ~ "Doméstica/o diarista",#"Domésticas/os diaristas",
  pos_ocup == 10 ~ "Trabalhador Familiar",
  pos_ocup == 11 ~ "Dono de negócio familiar",
  pos_ocup == 12 ~ "Outras posições",
  T ~ "não se aplica"))



#### Posição na ocupação - detalhada



pmb2022 %<>%mutate(setor = case_when(
  
  AAMM<201000 & (setor == 200 | (setor > 1999 & setor < 2999))~ "Indústria de Transformação", 
  AAMM<201000 & (setor == 300 | (setor > 2999 & setor < 3999)) ~ "Construção Civil", 
  AAMM<201000 & (setor == 400 | (setor > 3999 & setor < 4999))~ "Comércio",
  AAMM<201000 & (setor == 500 | (setor > 4999 & setor < 5110))~ "Serviços",
  AAMM<201000 & (setor == 511 | (setor > 5109 & setor < 6000))~ "Serviços",
  AAMM<201000 & (setor == 600 | setor > 5999) ~ "Demais Setores",
  
  AAMM>200900 & setor == 2000  ~ "Indústria de Transformação",
  AAMM>200900 & setor == 3000 ~ "Construção Civil",
  AAMM>200900 & setor == 4000 ~ "Comércio",
  AAMM>200900 & setor == 5000 ~ "Serviços" ,
  AAMM>200900 & setor == 9999 ~ "Demais Setores",
  AAMM>200900 & setor == 10000 ~ "Não ocupado",
  
  T ~ "não se aplica/NA"))


#### Grau de instrução

pmb2022 %<>% 
  mutate(instrucao = case_when(
    INST == 0 ~ "Não classificado",
    INST == 1 ~ "Sem declaração",
    INST == 2 ~ "Analfabeto",
    INST == 3 ~ "Alfabetizado s/ escolaridade",
    INST == 4 ~ "Fundamental incompleto",
    INST == 5 ~ "Fundamental completo",
    INST == 6 ~ "Médio incompleto",
    INST == 7 ~ "Médio completo",
    INST == 8 ~ "Superior incompleto",
    INST == 9 ~ "Superior completo",
    INST == 10 ~ "Pós-graduação"
    
  ))


#### Grau de instrução agregado
pmb2022 %<>% 
  mutate(instrucao_ag = case_when(
    INST == 0 ~ "Não classificado",
    INST == 1 ~ "Sem declaração",
    INST > 1 & INST <7  ~ "Até Ensino Médio Incompleto",
    INST > 6 & INST <9  ~ "Ensino Médio Completo/Superior Incompleto",
    INST > 8  ~ "No mínimo Superior Completo"
    
  ))




#### Manipulações para renda do trabalho cf documentações da PED

pmb2022 %<>% mutate(rend_trab_pr = case_when( AAMM< 201607 ~ F481,
                                              T ~ F470))

pmb2022%<>%mutate(rend_trab_pr = case_when(
  pos_ocup < 5 & rend_trab_pr == 0 ~ -1000,
  pos_ocup == 8 & rend_trab_pr == 0 ~ -1000,
  AAMM < 201607 & F470 > 9999999 & F481 > 9999999 ~ -1000,  #Q421 foi renomeada
  AAMM < 201607 & F470 > 9999999 & F481 < 9999999 ~ F481,  #Q421 foi renomeada
  AAMM < 201607 & pos_ocup == 10 ~ -1000,
  AAMM < 201607 & (F220 == 3 | F220 == 8) ~ -1000, #Q300 foi renomeada
  AAMM > 201606 & (F220 == 3 | F220 == 6) ~ -1000, #Q300 foi renomeada
  AAMM > 201606 &  F470 == 1000000000 & F481< 1000000000 ~ F481, #F320 foi renomeada
  AAMM > 201606 &  F470 == 1000000000 & F481 > 999999999 ~ -1000, #F320 foi renomeada
  AAMM < 201800 & AAMM > 201500 &  F210 == 13 ~ -1000, 
  AAMM > 201700 & F210 == 12 ~ -1000, 
  
  AAMM > 201606 & F470 == 1000000000 ~ -1000,
  T ~ rend_trab_pr
))

# pmb2022%<>%mutate(across(cols_ad$cod2018,~ifelse((AAMM<201607 & . >= 10000000) | (AAMM>201606 & . >= 1000000000), 0,
#                                                  .)))

###Renda do trabalho adicional

pmb2022%<>%mutate(renda_trab_ad = F65012+F65022)

pmb2022%<>%mutate(renda_trab_ad = case_when(
  F65012 > 9999999 & F65022 > 9999999 ~ 0,
  F65012 > 9999999 & F65022 < 10000000 ~ F65022,
  F65022 > 9999999 & F65012 < 10000000 ~ F65012,
  # pos_ocup < 5 & renda_trab_ad == 0 ~ -1000,
  # pos_ocup == 8 & renda_trab_ad == 0 ~ -1000,
  # AAMM < 201600 & (F220 == 3 | F220 == 8) ~ -1000, #Q300 foi renomeada
  # AAMM > 201500 & (F220 == 3 | F220 == 6) ~ -1000, #Q300 foi renomeada
  # AAMM < 201800 & AAMM > 201500 &  F210 == 13 ~ -1000, 
  # AAMM > 201700 & F210 == 12 ~ -1000, 
  # AAMM > 2015 & F470 == 1000000000 ~ -1000,
  T ~ renda_trab_ad
))


##Rem Indireta
pmb2022$rem_indireta_trab  <- rowSums(pmb2022%>%select(cols_ad[cols_ad$classificacao==3,]$cod2018),na.rm=T)



##Remuneração diferida
pmb2022$rem_diferida_trab  <- rowSums(pmb2022%>%select(cols_ad[cols_ad$classificacao==4,]$cod2018),na.rm=T)

###Outros rendimentos
pmb2022$outras_rendas  <- rowSums(pmb2022%>%select(cols_ad[cols_ad$classificacao==5,]$cod2018),na.rm=T)

#### Manipulações para renda do trabalho cf documentações da PED




saveRDS(pmb2022,"dados/ped_pmb_todos.rds")

source("R/anexo_anual_ped_e_deflatores_externos.R")

